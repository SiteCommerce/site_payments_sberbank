<?php

namespace Drupal\site_payments_sberbank\Plugin\PaymentSystem;

use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\site_payments\PaymentSystemPluginBase;
use GuzzleHttp\Client as Guzzle;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\HttpClient\GuzzleAdapter;
use Voronkovich\SberbankAcquiring\OrderStatus;
use Drupal\site_payments\TransactionInterface;
use Drupal\kvantstudio\Validator;

/**
 * @PaymentSystem(
 *   id="site_payments_sberbank",
 *   label = @Translation("Sberbank"),
 *   payment_method_name = @Translation("Payment by card online (Sberbank)")
 * )
 */
final class Sberbank extends PaymentSystemPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaymentSettings(?int $payment_account_id = NULL): array {
    $values = \Drupal::state()->getMultiple([
      'site_payments.allow_test_payment',
      'site_payments.test_hostnames',
      'site_payments.test_usernames',
      'site_payments_sberbank.block_rules_payment_system'
    ]);

    $array_state = [
      'allow_test_payment' => (bool) ($values['site_payments.allow_test_payment'] ?? FALSE),
      'test_hostnames' => $values['site_payments.test_hostnames'] ?? NULL,
      'test_usernames' => $values['site_payments.test_usernames'] ?? NULL,
      'block_rules_payment_system' => (int) ($values['site_payments_sberbank.block_rules_payment_system'] ?? 0),
    ];

    $array_account = $this->getAccount($payment_account_id);
    if ($array_account) {
      return array_merge($array_state, $array_account);
    }

    return $array_state;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus(): bool {
    $settings = $this->getPaymentSettings();
    return (bool) ($settings['status'] ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRulesPaymentSystem(): array {
    $settings = $this->getPaymentSettings();

    $build = [];
    if ($settings['block_rules_payment_system']) {
      $entity_type = 'block_content';
      $view_mode = 'default';
      $block = $this->entityTypeManager->getStorage($entity_type)->load($settings['block_rules_payment_system']);
      $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
      $build = $view_builder->view($block, $view_mode);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebhookUrl(): string {
    // URL webhook для платежной системы.
    $route_name = 'site_payments_sberbank.payment_webhook';
    $url = Url::fromRoute($route_name);

    return $url->setAbsolute()->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId(TransactionInterface $transaction): int|string {
    $order_id = $transaction->getOrderId();
    $created = $transaction->getCreatedTime();

    return "{$order_id}-{$created}";
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentUrl(array $payment_data): string {
    // Link for payment URL page.
    $payment_url = '';

    // We determine which bank account to choose in the payment system.
    $payment_account_id = empty($payment_data['payment_account']) ? NULL : (int) $payment_data['payment_account'];

    // Payment system settings for a specific bank account.
    $config = $this->getPaymentSettings($payment_account_id);
    $payment_data['payment_account'] = $config['id'];

    // Current time.
    $request_time = \Drupal::time()->getRequestTime();

    // Expiration time of the payment link.
    $overdue = (int) empty($config['overdue']) ? 28800 : ($config['overdue'] * 3600);
    $payment_data['overdue'] = $request_time + $overdue;

    // The name of the payment plugin.
    if (empty($payment_data['payment_plugin'])) {
      $payment_data['payment_plugin'] = $this->getId();
    }

    // The name of the payment system.
    if (empty($payment_data['payment_system'])) {
      $payment_data['payment_system'] = $config['payment_system'];
    }

    // The cost must be rounded to two decimal places after the separator.
    $payment_data['price'] = round($payment_data['price'], 2);

    // Register a transaction.
    $transaction = $this->createTransaction($payment_data, TRUE);
    if (!$transaction) {
      $error_data = json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Error before generate payment URL. Transaction not created: {$error_data}.");
      return $payment_url;
    }

    // Change the price for transaction.
    $payment_data['price'] = $this->correctPaidNumber($transaction, $config);

    // Register receipt.
    $this->createReceipt($transaction);

    // Calculate the cost of the transaction, it must be an integer.
    $price = $transaction->getPrice(TRUE);
    $price = (int) ($price * 100);

    // Check if the cost exists, that it is greater than zero.
    $payment = [];
    if (Validator::comparingNumbers($price)) {
      // Generate a unique order number order_id -a sequence of Latin letters, numbers and symbols with a length not exceeding 32 characters.
      $order_id = $this->getOrderId($transaction);

      // Creates an order in the payment system.
      $params = [
        'order_id' => $order_id,
        'price' => $price,
        'currency' => $transaction->getPriceСurrencyСode(),
        'success_redirect' => empty($payment_data['success_redirect_route']) ? $this->getSuccessUrl($transaction->uuid()) : $this->getSuccessUrl($transaction->uuid(), $payment_data['success_redirect_route']),
        'failure_redirect' => empty($payment_data['failure_redirect_route']) ? $this->getFailureUrl($transaction->uuid()) : $this->getFailureUrl($transaction->uuid(), $payment_data['failure_redirect_route']),
        'webhook_redirect' => empty($payment_data['webhook_redirect']) ? $this->getWebhookUrl() : $payment_data['webhook_redirect'],
        'sessionTimeoutSecs' => $overdue,
        'login' => $config['login'],
        'password' => $config['password'],
        'mode' => $config['mode'],
        'verify_ssl' => $config['verify_ssl']
      ];
      $payment = $this->registerOrder($params);
    } else {
      $error_data = \json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Error before generate payment URL: {$error_data}.");
    }

    // Get a link to pay from the result of the request.
    if (!empty($payment['formUrl'])) {
      $payment_url = $payment['formUrl'];

      // Assign the data received from the payment system to the transaction.
      $transaction->setPaymentId($payment['orderId']);
      $transaction->setPaymentLink($payment['formUrl']);
      $transaction->setPaymentStatus('issued');
      $transaction->save();
    } else {
      $error_code = empty($payment['errorCode']) ? 'not set' : $payment['errorCode'];
      $error_message = empty($payment['errorMessage']) ? 'not set' : $payment['errorMessage'];
      $this->logger->error("Error while generate payment URL. Code: {$error_code}. Message: {$error_message}.");
    }

    return $payment_url;
  }

  /**
   * Creates an order in the payment system.
   *
   * @param array $data
   * @return array|null
   */
  private function registerOrder(array $data): ?array {
    $result = NULL;

    try {
      // Client for working with Sberbanks's aquiring REST API.
      $client = new Client([
        'userName' => $data['login'],
        'password' => $data['password'],
        'httpClient' => new GuzzleAdapter(new Guzzle(['verify' => $data['verify_ssl']])),
        'apiUri' => $data['mode'] ? Client::API_URI : Client::API_URI_TEST,
      ]);

      // Determine the numeric code of the currency.
      $currency = Currency::RUB;
      switch ($data['currency']) {
        case 'EUR':
          $currency = Currency::EUR;
          break;
        case 'UAH':
          $currency = Currency::UAH;
          break;
        case 'USD':
          $currency = Currency::USD;
          break;
      }

      // An order identifier.
      $orderId = $data['order_id'];

      // An order amount.
      $orderAmount = $data['price'];

      // An url for redirecting a user after successfull order handling.
      $returnUrl = $data['success_redirect'];

      // Additional data.
      $params['currency'] = $currency;
      $params['failUrl'] = $data['failure_redirect'];
      $params['sessionTimeoutSecs'] = $data['sessionTimeoutSecs'];

      $result = $client->registerOrder($orderId, $orderAmount, $returnUrl, $params);
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  function checkPaymentStatus(TransactionInterface $transaction, array $settings = []): null|string {
    // Настройки платежной системы.
    $payment_account = $transaction->getPaymentAccountId();
    $config = array_merge($this->getPaymentSettings($payment_account), $settings);

    $result = NULL;

    try {
      $client = new Client([
        'userName' => $config['login'],
        'password' => $config['password'],
        'httpClient' => new GuzzleAdapter(new Guzzle()),
        'apiUri' => $config['mode'] ? Client::API_URI : Client::API_URI_TEST,
      ]);

      $payment_id = $transaction->getPaymentId();
      $order = $client->getOrderStatus($payment_id);

      if (OrderStatus::isDeposited($order['orderStatus'])) {
        $result = 'paid';
      }
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }
}
