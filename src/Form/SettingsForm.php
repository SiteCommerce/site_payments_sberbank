<?php

namespace Drupal\site_payments_sberbank\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class Form of Sberbank settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Sate API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_payments_sberbank_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_payments_sberbank.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Настройки.
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    $block_rules_payment_system = (int) $this->state->get('site_payments_sberbank.block_rules_payment_system');
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_rules_payment_system);
    $form['settings']['block_rules_payment_system'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Rules for the use of the payment system'),
      '#target_type' => 'block_content',
      '#default_value' => $block,
      '#description' => $this->t('You need to create a custom block and select it in this field. The block content is displayed under the select payment method field.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Правила использования платежной системы.
    $block_rules_payment_system = trim($form_state->getValue('block_rules_payment_system'));
    $this->state->set('site_payments_sberbank.block_rules_payment_system', $block_rules_payment_system);
  }
}
