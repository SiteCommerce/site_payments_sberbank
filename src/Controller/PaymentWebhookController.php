<?php

namespace Drupal\site_payments_sberbank\Controller;

use Drupal\Core\Controller\ControllerBase;

class PaymentWebhookController extends ControllerBase {

  /**
   * Страница с уведомлением после успешной оплаты.
   */
  public function paymentWebhook() {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
